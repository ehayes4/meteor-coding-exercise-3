/*****************************************************************************/
/* PlayerResponse: Event Handlers and Helpersss .js*/
/*****************************************************************************/
Template.PlayerResponse.events({
  /*
   * Example:
   *  'click .selector': function (e, tmpl) {
   *
   *  }
   */
});

Template.PlayerResponse.helpers({
  player_response: function () {
    return PlayerResponse.find({});
    }
  /*
   * Example:
   *  items: function () {
   *    return Items.find();
   *  }
   */
});

/*****************************************************************************/
/* PlayerResponse: Lifecycle Hooks */
/*****************************************************************************/
Template.PlayerResponse.created = function () {
};

Template.PlayerResponse.rendered = function () {
};

Template.PlayerResponse.destroyed = function () {
};