/*****************************************************************************/
/* PlayerRequest: Event Handlers and Helpersss .js*/
/*****************************************************************************/
Template.PlayerRequest.events({
  /*
   * Example:
   *  'click .selector': function (e, tmpl) {
   *
   *  }
   */
});

Template.PlayerRequest.helpers({
  player_request: function () {
    return PlayerRequest.find({});
    }
  /*
   * Example:
   *  items: function () {
   *    return Items.find();
   *  }
   */
});

/*****************************************************************************/
/* PlayerRequest: Lifecycle Hooks */
/*****************************************************************************/
Template.PlayerRequest.created = function () {
};

Template.PlayerRequest.rendered = function () {
};

Template.PlayerRequest.destroyed = function () {
};