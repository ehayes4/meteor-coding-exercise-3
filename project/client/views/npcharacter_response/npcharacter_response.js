/*****************************************************************************/
/* NpcharacterResponse: Event Handlers and Helpersss .js*/
/*****************************************************************************/
Template.NpcharacterResponse.events({
  /*
   * Example:
   *  'click .selector': function (e, tmpl) {
   *
   *  }
   */
});

Template.NpcharacterResponse.helpers({
  npcharacter_response: function () {
    return NPCharacterResponse.find({});
    }
  /*
   * Example:
   *  items: function () {
   *    return Items.find();
   *  }
   */
});

/*****************************************************************************/
/* NpcharacterResponse: Lifecycle Hooks */
/*****************************************************************************/
Template.NpcharacterResponse.created = function () {
};

Template.NpcharacterResponse.rendered = function () {
};

Template.NpcharacterResponse.destroyed = function () {
};