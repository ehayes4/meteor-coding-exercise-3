/*****************************************************************************/
/* NpcharacterRequest: Event Handlers and Helpersss .js*/
/*****************************************************************************/
Template.NpcharacterRequest.events({
  /*
   * Example:
   *  'click .selector': function (e, tmpl) {
   *
   *  }
   */
});

Template.NpcharacterRequest.helpers({
  npcharacter_request: function () {
    return NPCharacterRequest.find({});
    }
  /*
   * Example:
   *  items: function () {
   *    return Items.find();
   *  }
   */
});

/*****************************************************************************/
/* NpcharacterRequest: Lifecycle Hooks */
/*****************************************************************************/
Template.NpcharacterRequest.created = function () {
};

Template.NpcharacterRequest.rendered = function () {
};

Template.NpcharacterRequest.destroyed = function () {
};