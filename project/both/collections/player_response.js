PlayerResponse = new Mongo.Collection('player_response');

PlayerResponse.attachSchema(new SimpleSchema({


  title:{
    type:String
  },
  scenarioIds : {
    type:[String],
    optional: true
  },
  playerRequestIds : {
    type:[String],
    optional: true
  },
  playerResponseType : {
    type:[String],
    optional: true
  }
  
}));

/*
 * Add query methods like this:
 *  PlayerResponse.findPublic = function () {
 *    return PlayerResponse.find({is_public: true});
 *  }
 */