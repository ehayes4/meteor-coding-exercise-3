NPCharacterResponse = new Mongo.Collection('n_p_character_response');

NPCharacterResponse.attachSchema(new SimpleSchema({

  title:{
    type:String
  },
  scenarioIds : {
    type:[String],
    optional: true
  },
  npCharacterRequestIds : { /*requestIds : {*/
    type:[String],
    optional: true
  },
  npCharacterResponseType : {
    type:[String],
    optional: true
  },

}));
/*
 * Add query methods like this:
 *  NPCharacterResponse.findPublic = function () {
 *    return NPCharacterResponse.find({is_public: true});
 *  }
 */