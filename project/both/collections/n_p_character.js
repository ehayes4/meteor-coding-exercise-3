NPCharacter = new Mongo.Collection('n_p_character');

NPCharacter.attachSchema(new SimpleSchema({

  title:{
    type:String
  },
  npCharacterIds:{
    type:[String],
    optional: true
  },
  npCharacterLevel:{
    type:[Number],
    optional: true
  }

}));