/*****************************************************************************/
/* Client and Server Routes */
/*****************************************************************************/
Router.configure({
  layoutTemplate: 'MasterLayout',
  loadingTemplate: 'Loading',
  notFoundTemplate: 'NotFound'
});

Router.route('/', {name: 'home'});
Router.route('/scenarios', {name: 'scenarios'});
Router.route('/npcharacter', {name: 'npcharacter'});
Router.route('/npcharacter_request', {name: 'npcharacter_request'});
Router.route('/npcharacter_response', {name: 'npcharacter_response'});
Router.route('/player_request', {name: 'player_request'});
Router.route('/player_response', {name: 'player_response'});